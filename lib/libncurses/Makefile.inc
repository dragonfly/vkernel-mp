# $DragonFly: src/lib/libncurses/Makefile.inc,v 1.6 2005/09/22 13:14:44 asmodai Exp $

NCURSESDIR=	${.CURDIR}/../../../contrib/ncurses-5.4

CFLAGS+=	-I${NCURSESDIR}/include/
CFLAGS+=	-I${.CURDIR}/../include/
CFLAGS+=	-I${.CURDIR} -I${.OBJDIR}
CFLAGS+=	-D_XOPEN_SOURCE_EXTENDED

SHLIB_MAJOR=	6
