.\"
.\" Copyright (c) 2006 The DragonFly Project.  All rights reserved.
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in
.\"    the documentation and/or other materials provided with the
.\"    distribution.
.\" 3. Neither the name of The DragonFly Project nor the names of its
.\"    contributors may be used to endorse or promote products derived
.\"    from this software without specific, prior written permission.
.\" 
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
.\" ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
.\" LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
.\" FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
.\" COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING,
.\" BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
.\" LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
.\" AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\" OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
.\" OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man7/release.7,v 1.3.2.6 2003/04/12 20:42:30 murray Exp $
.\" $DragonFly: src/share/man/man7/release.7,v 1.8 2006/07/26 22:18:54 swildner Exp $
.\"
.Dd July 26, 2006
.Dt RELEASE 7
.Os
.Sh NAME
.Nm release
.Nd "release building infrastructure"
.Sh DESCRIPTION
.Dx
provides a complete build environment suitable for users to make
full releases of the
.Dx
operating system.
All of the tools necessary to build releases are available from the
CVS repository in
.Pa src/nrelease .
A complete release (that is, a live CD) can actually be built with
only a single command.
This command is aptly named
.Dq Li "make release"
(or
.Dq Li "make installer_release" ,
see below).
.Pp
This document does not cover source code management, quality
assurance, or other aspects of the release engineering process.
.Sh TARGETS
The release Makefile
.Pq Pa src/nrelease/Makefile
defines a number of targets which are roughly modeled after the
targets used for building the system.
.Bl -tag -width ".Cm realquickrel"
.It Cm release
Builds a vanilla live CD.
.It Cm quickrel
Same as
.Cm release
but uses the
.Cm quickworld
target.
.It Cm realquickrel
Same as
.Cm release
but uses the
.Cm realquickworld
target.
.It Cm clean
Removes the ISO root directory and object files.
.It Cm realclean
Same as
.Cm clean
but also removes external packages.
.El
.Pp
The
.Cm quickrel
and
.Cm realquickrel
targets will only work if a full
.Dq Li "make release"
has been done before.
For a description of the corresponding build targets, see
.Xr build 7 .
.Pp
The
.Cm release ,
.Cm quickrel
and
.Cm realquickrel
targets all have corresponding targets with
.Dq Li installer_
prefixed (e.g.,
.Cm installer_release )
which cause the BSD installer from
.Pa http://www.bsdinstaller.org
to be added to the resulting ISO image.
.Pp
During the process of rolling a release a check is performed to find
out if a number of external packages to be put on the CD exist on
your system.
If they cannot be found, the release process will stop and give
proper commands for downloading and setting up these packages.
.Sh ENVIRONMENT
.Bl -tag -width ".Va WITHOUT_SRCS"
.It Va WITHOUT_SRCS
If set, this variable will prevent kernel source code from being copied
to
.Pa /usr/src-sys.tar.bz2
on the ISO image.
.El
.Sh FILES
.Bl -tag -compact
.It Pa /etc/make.conf
.It Pa src/Makefile
.It Pa src/Makefile.inc1
.It Pa src/nrelease/Makefile
.El
.Sh EXAMPLES
The following sequence of commands is used to build a standard
.Dx 1.4
installation CD image (assuming that a local CVS repository is
available):
.Bd -literal -offset indent
cd /usr
cvs co -rDragonFly_RELEASE_1_4 src
cd src/nrelease
make installer_release
.Ed
.Pp
After running these commands, the image will be available in the
.Pa /usr/release
directory.
.Sh SEE ALSO
.Xr cvs 1 ,
.Xr make 1 ,
.Xr make.conf 5 ,
.Xr build 7
.Sh HISTORY
.Pa src/nrelease
was added in
.Dx 1.0
to implement the idea of a
.Dq "live CD" .
.Sh AUTHORS
.An -nosplit
The
.Nm
infrastructure was written by
.An Matthew Dillon .
This manual page was written by
.An Sascha Wildner .
.Sh BUGS
There are more targets and variables in
.Pa src/nrelease/Makefile
that should be documented.
