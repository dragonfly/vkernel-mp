/* 
 * $NetBSD: math.h,v 1.2 2003/10/28 00:55:28 kleink Exp $
 * $DragonFly: src/sys/cpu/i386/include/math.h,v 1.2 2006/11/07 18:51:21 dillon Exp $
 */

#ifndef _CPU_MATH_H_
#define _CPU_MATH_H_

#define	__HAVE_LONG_DOUBLE
#define	__HAVE_NANF

#endif
