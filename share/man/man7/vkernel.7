.\"
.\" Copyright (c) 2003, 2004, 2005, 2006, 2007
.\"	The DragonFly Project.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\"
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in
.\"    the documentation and/or other materials provided with the
.\"    distribution.
.\" 3. Neither the name of The DragonFly Project nor the names of its
.\"    contributors may be used to endorse or promote products derived
.\"    from this software without specific, prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
.\" ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
.\" LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
.\" FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
.\" COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING,
.\" BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
.\" LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
.\" AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\" OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
.\" OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $DragonFly: src/share/man/man7/vkernel.7,v 1.21 2007/06/23 20:52:41 swildner Exp $
.\"
.Dd June 14, 2007
.Dt VKERNEL 7
.Os
.Sh NAME
.Nm vkernel ,
.Nm vkd ,
.Nm vke
.Nd virtual kernel architecture
.Sh SYNOPSIS
.Cd "platform vkernel"
.Cd "device vkd"
.Cd "device vke"
.Pp
.Pa /usr/obj/usr/src/sys/VKERNEL/kernel.debug
.Op Fl sUv
.Op Fl c Ar file
.Op Fl e Ar name Ns = Ns Li value : Ns Ar name Ns = Ns Li value : Ns ...
.Op Fl i Ar file
.Op Fl I Ar interface Ns Op Ar :address1 Ns Oo Ar :address2 Oc Ns Oo Ar /netmask Oc
.Op Fl m Ar size
.Op Fl p Ar file
.Op Fl r Ar file
.Sh DESCRIPTION
The
.Nm
architecture allows for running
.Dx
kernels in userland.
.Pp
The following options are available:
.Bl -tag -width ".Fl m Ar size"
.It Fl c Ar file
Specify a readonly CD-ROM image
.Ar file
to be used by the kernel, with the first
.Fl c
option defining
.Li vcd0 ,
the second one
.Li vcd1 ,
and so on.  The first
.Fl r
or
.Fl c
option specified on the command line will be the boot disk.
The CD9660 filesystem is assumed when booting from this media.
.It Fl e Ar name Ns = Ns Li value : Ns Ar name Ns = Ns Li value : Ns ...
Specify an environment to be used by the kernel.
.It Fl i Ar file
Specify a memory image
.Ar file
to be used by the virtual kernel.
If no
.Fl i
option is given, the kernel will generate a name of the form
.Pa /var/vkernel/memimg.XXXXXX ,
with the trailing
.Ql X Ns s
being replaced by a sequential number, e.g.\&
.Pa memimg.000001 .
.It Fl I Ar interface Ns Op Ar :address1 Ns Oo Ar :address2 Oc Ns Oo Ar /netmask Oc
Create a virtual network device, with the first
.Fl I
option defining
.Li vke0 ,
the second one
.Li vke1 ,
and so on.
.Pp
The
.Ar interface
argument is the name of a
.Xr tap 4
device node.
The
.Pa /dev/
path prefix does not have to be specified and will be automatically prepended.
Specifying
.Cm auto
will pick the first unused
.Xr tap 4
device.
.Pp
The
.Ar address1
and
.Ar address2
arguments are the IP addresses of the
.Xr tap 4
and
.Nm vke
interfaces.
Optionally,
.Ar address1
may be of the form
.Li bridge Ns Em X
in which case the
.Xr tap 4
interface is added to the specified
.Xr bridge 4
interface.
.Pp
The
.Ar netmask
argument applies to all interfaces for which an address is specified.
.It Fl m Ar size
Specify the amount of memory to be used by the kernel in bytes,
.Cm K
.Pq kilobytes ,
.Cm M
.Pq megabytes
or
.Cm G
.Pq gigabytes .
Lowercase versions of
.Cm K , M ,
and
.Cm G
are allowed.
.It Fl p Ar file
Specify a file in which to store the process ID.
A warning is issued if this file cannot be opened for writing.
.It Fl r Ar file
Specify a R/W disk image
.Ar file
to be used by the kernel, with the first
.Fl r
option defining
.Li vkd0 ,
the second one
.Li vkd1 ,
and so on.  The first
.Fl r
or
.Fl c
option specified on the command line will be the boot disk.
.It Fl s
Boot into single-user mode.
.It Fl U
Enable writing to kernel memory and module loading.
By default, those are disabled for security reasons.
.It Fl v
Turn on verbose booting.
.El
.Sh DEVICES
A number of virtual device drivers exist to supplement the virtual kernel.
Their names start with
.Li vk .
.Ss Disk device
The
.Nm vkd
driver allows for up to 16
.Xr vn 4
based disk devices.
The root device will be
.Li vkd0
(see
.Sx EXAMPLES
for further information on how to prepare a root image).
.Ss Network interface
The
.Nm vke
driver supports up to 16 virtual network interfaces which are associated with
.Xr tap 4
devices on the host.
For each
.Nm vke
device, the per-interface read only
.Xr sysctl 3
variable
.Va hw.vke Ns Em X Ns Va .tap_unit
holds the unit number of the associated
.Xr tap 4
device.
.Sh SIGNALS
The virtual kernel only enables
.Dv SIGQUIT
and
.Dv SIGTERM
while operating in regular console mode.
Sending
.Ql \&^\e
.Pq Dv SIGQUIT
to the virtual kernel causes the virtual kernel to enter its internal
.Xr ddb 4
debugger and re-enable all other terminal signals.
Sending
.Dv SIGTERM
to the virtual kernel triggers a clean shutdown by passing a
.Dv SIGUSR2
to the virtual kernel's
.Xr init 8
process.
.Sh DEBUGGING
It is possible to directly gdb the virtual kernel's process.
It is recommended that you do a
.Ql handle SIGSEGV noprint
to ignore page faults processed by the virtual kernel itself.
.Sh EXAMPLES
A couple of steps are necessary in order to prepare the system to build and
run a virtual kernel.
.Ss Setting up the filesystem
The
.Nm
architecture needs a number of files which reside in
.Pa /var/vkernel .
Since these files tend to get rather big and the
.Pa /var
partition is usually of limited size, we recommend the directory to be
created in the
.Pa /home
partition with a link to it in
.Pa /var :
.Bd -literal
mkdir /home/var.vkernel
ln -s /home/var.vkernel /var/vkernel
.Ed
.Pp
Next, a filesystem image to be used by the virtual kernel has to be
created and populated (assuming world has been built previously):
.Bd -literal
dd if=/dev/zero of=/var/vkernel/rootimg.01 bs=1m count=2048
vnconfig -c -s labels vn0 /var/vkernel/rootimg.01
disklabel -r -w vn0s0 auto
disklabel -e vn0s0	# edit the label to create a vn0s0a partition
newfs /dev/vn0s0a
mount /dev/vn0s0a /mnt
cd /usr/src
make installworld DESTDIR=/mnt
cd etc
make distribution DESTDIR=/mnt
echo '/dev/vkd0a / ufs rw 1 1' >/mnt/etc/fstab
.Ed
.Pp
Edit
.Pa /mnt/etc/ttys
and replace the
.Li console
entry with the following line and turn off all other gettys.
.Bd -literal
console	"/usr/libexec/getty Pc"		cons25	on  secure
.Ed
.Pp
Then, unmount the disk.
.Bd -literal
umount /mnt
vnconfig -u vn0
.Ed
.Ss Compiling the virtual kernel
In order to compile a virtual kernel use the
.Li VKERNEL
kernel configuration file residing in
.Pa /usr/src/sys/config
(or a configuration file derived thereof):
.Bd -literal
cd /usr/src
make -DNO_MODULES buildkernel KERNCONF=VKERNEL
.Ed
.Ss Enabling virtual kernel operation
A special
.Xr sysctl 8 ,
.Va vm.vkernel_enable ,
must be set to enable
.Nm
operation:
.Bd -literal
sysctl vm.vkernel_enable=1
.Ed
.Ss Configuring the network on the host system
In order to access a network interface of the host system from the
.Nm ,
you must add the interface to a
.Xr bridge 4
device which will then be passed to the
.Fl I
option:
.Bd -literal
kldload if_bridge.ko
kldload if_tap.ko
ifconfig bridge0 create
ifconfig bridge0 addm re0	# assuming re0 is the host's interface
ifconfig bridge0 up
.Ed
.Ss Running the kernel
Finally, the virtual kernel can be run:
.Bd -literal
cd /usr/obj/usr/src/sys/VKERNEL
\&./kernel.debug -m 64m -r /var/vkernel/rootimg.01 -I auto:bridge0
.Ed
.Pp
The
.Xr reboot 8
command can be used to stop a virtual kernel.
.Sh SEE ALSO
.Xr bridge 4 ,
.Xr tap 4 ,
.Xr vn 4 ,
.Xr build 7 ,
.Xr disklabel 8 ,
.Xr ifconfig 8 ,
.Xr vnconfig 8
.Sh HISTORY
Virtual kernels were introduced in
.Dx 1.7 .
.Sh AUTHORS
.An -nosplit
.An Matt Dillon
thought up and implemented the
.Nm
architecture and wrote the
.Nm vkd
device driver.
.An Sepherosa Ziehau
wrote the
.Nm vke
device driver.
This manual page was written by
.An Sascha Wildner .
