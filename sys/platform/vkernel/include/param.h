/*
 * $DragonFly: src/sys/platform/vkernel/include/param.h,v 1.1 2007/01/19 08:23:43 dillon Exp $
 */

#ifndef _MACHINE_PARAM_H_

#ifndef _NO_NAMESPACE_POLLUTION
#define _MACHINE_PARAM_H_
#endif

#ifndef _MACHINE_PLATFORM
#define _MACHINE_PLATFORM	vkernel
#endif

#ifndef _NO_NAMESPACE_POLLUTION

#ifndef MACHINE_PLATFORM
#define MACHINE_PLATFORM	"vkernel"
#endif

#endif

#include <cpu/param.h>

#endif

