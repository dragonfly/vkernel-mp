.\" Copyright (c) 2003,2004 The DragonFly Project.  All rights reserved.
.\" 
.\" This code is derived from software contributed to The DragonFly Project
.\" by Matthew Dillon <dillon@backplane.com>
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in
.\"    the documentation and/or other materials provided with the
.\"    distribution.
.\" 3. Neither the name of The DragonFly Project nor the names of its
.\"    contributors may be used to endorse or promote products derived
.\"    from this software without specific, prior written permission.
.\" 
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
.\" ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
.\" LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
.\" FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
.\" COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING,
.\" BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
.\" LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
.\" AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\" OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
.\" OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\" 
.\" $DragonFly: src/share/man/man7/committer.7,v 1.5 2007/05/13 20:29:48 hasso Exp $
.\" 
.Dd January 1, 2007
.Dt COMMITTER 7
.Os
.Sh NAME
.Nm committer
.Nd Rules for
.Dx
committers
.Sh CVS REPOSITORY ON YOUR LOCAL MACHINE
Use
.Xr cvsup 1
to mirror the
.Dx
CVS repository itself onto your local box
(if you haven't already).  See the file
.Pa /usr/share/examples/cvsup/DragonFly-cvs-supfile .
.Pp
You basically want to do all CVS operations not related to commits
via your local repository, and do commit operations directly to
the master site.
.Pp
We strongly recommend that you set up a
.Xr cron 8
job to automatically
cvsup at least once a day, so your local CVS repository is always
in sync.
.Sh CHECKING OUT THE SOURCES
Your
.Pa ~/.cvsrc
should contain:
.Bd -literal -offset indent
cvs -q
diff -u
update -Pd
checkout -P
.Ed
.Pp
Checking out and updating a checked out source tree:
.Bd -literal -offset indent
cd /usr
cvs -d /home/dcvs checkout src
cvs -d /home/dcvs update src
.Ed
.Pp
We do
.Em NOT
recommend that you use a cron job to
.Nm cvs Cm update
your
checked out source.  Automatic updates can interfere with
any work-in-progress that you have.
.Sh SSH DSA KEYS:
The CVS repository machine is
.Pa crater.dragonflybsd.org ,
and the
.Dx
developer machine is
.Pa leaf.dragonflybsd.org .
We create
an account for you on both machines and install your public SSH
key to give you access.
.Pp
Your
.Pa crater
account is set up for CVS repository only. It can
only operate as a CVS slave and cannot be logged into.  That is,
.Pa crater.dragonflybsd.org
is only used as part of
.Nm cvs Fl d Ar ...
operations.
.Pp
Your
.Pa leaf
account is a general developer account.  Any
.Dx
developer can have a
.Pa leaf
account, whether a committer or not.
It can be useful as a developer rendezvous,
however.  For example, people upload kernel cores to
.Pa leaf
so other
developers can look at them.  You log into your
.Pa leaf
account with:
.Pp
.Li ssh you@leaf.dragonflybsd.org
.Pp
The rules for account use are in
.Pa leaf Ap s
MOTD.
It is very important that you never install a password or create a SSH
key pair on
.Pa leaf
to use to access other machines.
Because non-committers can have
.Pa leaf
accounts, since
.Pa leaf
is not considered
a secure machine.
.Sh TESTING COMMIT ACCESS
There is a directory called
.Pa /usr/src/test/test .
To test your commit
access, try making a modification and committing a file in this
directory.
.Pp
.Bd -literal -offset indent
cd /usr/src/test/test
(edit something)
cvs -d you@crater.dragonflybsd.org commit file_you_edited
.Ed
.Sh COMITTING REAL WORK
Make modifications as needed.  For example, edit files.  If adding
new files make CVS aware of them like this.  Files can just be
added locally, but directories have to run through
.Pa crater .
These
operations do not actually effect the repository (except directories
being added are mkdir'd in the repository).  Instead they are
stored in the checkout source's
.Pa CVS/
subdirectory and then
synchronized to the repository when you
.Nm cvs Cm commit .
.Pp
.Bd -literal -offset indent
cvs add filename
cvs -d you@crater.dragonflybsd.org:/cvs add directory
.Ed
.Pp
To commit to the repository, use:
.Bd -literal -offset indent
cvs -d you@crater.dragonflybsd.org:/cvs commit files_or_directories
.Ed
.Pp
Do not set
.Ev CVSROOT
to
.Pa you@crater.dragonflybsd.org:/cvs .
The reason is that you want the default
.Ev CVSROOT
in your checked out
sources to point at your local CVS repository, not at
.Pa crater .
This reduces instances where accidental commits or repository
operations are made on
.Pa crater .
.Pp
It is best to avoid
.Nm cvs Cm update Ap ing
directly from the repository.
e.g. try to avoid doing
.Nm cvs Fl d Ar ... Cm update
directly from
.Pa crater .
Instead, use
.Xr cvsup 1
to resync your local copy of the repository
and use
.Nm cvs Cm update
or
.Nm cvs Fl d Ar /home/dcvs Cm update
to update from
your local copy of the repository.
.Pp
The idea here is to try to avoid having CVS set its
.Pa CVS/Root
file in any given checked out CVS directory to point at
.Pa crater .
You really want it to just point at your local copy of the CVS
repository.
.Pp
Never do
.Nm cvs Cm tag
or
.Cm rtag
operations.  Such operations can be
very dangerous and only highly experienced CVS admins should
do them.  That's basically just two or three people (Matt and Joerg
primarily).
.Pp
The best way to resynchronize your local CVS repository after
making a commit is to cvsup again.
.Sh DISCUSSING COMMITTABLE WORK BEFORE HAND
Discussion prior to commit usually occurs on the kernel@, submit@, or bugs@
mailing lists.  It depends on the work involved.  Simple and obvious work,
such as documentation edits or additions, don't really need a head's up.
.Pp
Simple and obvious bug fixes don't need a head's up, other than to
say that you will (or just have) committed the fix, so you don't
race other committers trying to do the same thing.  Usually the
developer most active in a discussion about a bug commits the
fix, but it isn't considered a big deal.
.Pp
More complex issues are usually discussed on the lists first.
Non-trivial but straight forward bug fixes usually go through
a testing period, where you say something like:
.Do
Here is a patch
to driver BLAH that fixes A, B, and C, please test it.  If there
are no objections I will commit it next Tuesday.
.Dc
(usually a week,
or more depending on the complexity of the patch).
.Pp
New drivers or utilities are usually discussed.  Committers will
often commit new work
.Em without
hooking it into the buildworld or
buildkernel infrastructure in order to be able to continue
development on it in piecemeal without having to worry about it
breaking buildworld or buildkernel, and then they hook it in as a
last step after they've stabilized it.  Examples of this include
new versions of GCC, updates to vendor packages such as bind,
sendmail, etc.
.Sh DEVELOPER LOCKS
Areas within the CVS repository are never explicitly locked.
Often situations will arise where one developer commits work and
another developer finds an issue with it that needs to be corrected.
.Pp
All committed work becomes community property.  No developer has a
.Sq lock
on any part of the source tree.  However, if a developer is
actively working on a portion of the source tree and you find a bug
or other issue, courtesy dictates that you post to kernel@ and/or
email the developer.
.Pp
This means that, generally, if you do not see a commit to an area
of the source tree in the last few weeks, it isn't considered active and
you don't really need to confer with the developer that made the
commit, though you should still post to the kernel@ mailing list
and, of course, confer with developers when their expertise is
needed.
.Pp
One exception to this rule is documentation.  If any developer commits
new work, the documentation guys have free reign to go in and
correct
.Xr mdoc 7
errors.  This is really a convenience as most developers
are not
.Xr mdoc 7
gurus and it's a waste of time for the doc guys to post
to kernel@ for all the little corrections they make.
.Sh CONFLICTS
On the occasion that a major code conflict occurs, for example if two
people are doing major work in the same area of the source tree and forgot
to collaborate with each other, the project leader will be responsible for
resolving the conflict.  Again, the repository is considered community
property and it must be acceptable for any developer to be able to work on
any area of the tree that he or she has an interest in.
.Sh MAJOR ARCHITECTURAL CHANGES
This is generally Matt Dillon's area of expertise.  All major architectural
changes must be discussed on the kernel@ mailing list and he retains
veto power.
.Pp
This isn't usually an issue with any work.  At best if something
doesn't look right architecturally he'll chip in with adjustments to
make it fit in.  Nothing ever really gets vetoed.
.Sh SEE ALSO
.Xr cvs 1 ,
.Xr cvsup 1 ,
.Xr development 7
