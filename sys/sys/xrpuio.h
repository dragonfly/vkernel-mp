/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.org> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 *
 * $FreeBSD: src/sys/sys/xrpuio.h,v 1.2 1999/08/28 00:52:12 peter Exp $
 * $DragonFly: src/sys/sys/xrpuio.h,v 1.3 2006/05/20 04:42:13 dillon Exp $
 *
 */

#ifndef _SYS_XRPUIO_H_
#define _SYS_XRPUIO_H_

#ifndef _SYS_TYPES_H_
#include <sys/types.h>
#endif
#ifndef _SYS_IOCCOM_H_
#include <sys/ioccom.h>
#endif

#define XRPU_MAX_PPS	16
struct xrpu_timecounting {

	/* The timecounter itself */
	u_int		xt_addr_trigger;
	u_int		xt_addr_latch;
	unsigned	xt_mask;
	u_int32_t	xt_frequency;
	char		xt_name[16];

	/* The PPS latches */
	struct {
		u_int	xt_addr_assert;
		u_int	xt_addr_clear;
	} xt_pps[XRPU_MAX_PPS];
};

#define XRPU_IOC_TIMECOUNTING _IOW('6', 1, struct xrpu_timecounting)

#endif /* _SYS_XRPUIO_H_ */
