/* $DragonFly: src/gnu/usr.bin/gdb/libgdb/version.c,v 1.1 2006/03/07 16:48:11 corecode Exp $ */
#include "version.h"
const char version[] = "6.2.1";
const char host_name[] = "i386-dragonfly";
const char target_name[] = "i386-dragonfly";
