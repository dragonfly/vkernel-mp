
/*
 * Copyright (c) 2006 The DragonFly Project.  All rights reserved.
 * 
 * This code is derived from software contributed to The DragonFly Project
 * by Matthew Dillon <dillon@backplane.com>
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name of The DragonFly Project nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific, prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * $DragonFly: src/sys/platform/vkernel/i386/exception.c,v 1.6 2007/06/17 16:46:15 dillon Exp $
 */

#include "opt_ddb.h"
#include <sys/types.h>
#include <sys/systm.h>
#include <sys/reboot.h>
#include <sys/kernel.h>
#include <sys/kthread.h>
#include <sys/reboot.h>
#include <ddb/ddb.h>

#include <sys/thread2.h>

#include <machine/trap.h>
#include <machine/md_var.h>
#include <machine/segments.h>

#include <err.h>
#include <signal.h>
#include <unistd.h>

int _ucodesel = LSEL(LUCODE_SEL, SEL_UPL);
int _udatasel = LSEL(LUDATA_SEL, SEL_UPL);

static void exc_segfault(int signo, siginfo_t *info, void *ctx);
#ifdef DDB
static void exc_debugger(int signo, siginfo_t *info, void *ctx);
#endif

/* signal shutdown thread misc. */

static void sigshutdown_daemon( void );
static struct thread *sigshutdown_thread;
static struct kproc_desc sigshut_kp = {
	"sigshutdown", sigshutdown_daemon, &sigshutdown_thread
};

static
void
ipi(int nada, siginfo_t *info, void *ctxp)
{
	struct globaldata *gd = mycpu;

	kprintf("ipi\n");
	++mycpu->gd_intr_nesting_level;
	if (IN_CRITICAL_SECT(curthread)) {
		kprintf("JAT: ipi in critical section\n");
		gd->gd_reqflags &= RQF_IPIQ;
		lwkt_process_ipiq();
	} else {
		kprintf("JAT: ipi NOT in critical section\n");
		curthread->td_pri += TDPRI_CRIT;
		lwkt_process_ipiq();
		curthread->td_pri -= TDPRI_CRIT;
	}
	--mycpu->gd_intr_nesting_level;
}


void
init_exceptions(void)
{
	struct sigaction sa;

	bzero(&sa, sizeof(sa));
	sa.sa_sigaction = exc_segfault;
	sa.sa_flags |= SA_SIGINFO;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGSEGV, &sa, NULL);
	sigaction(SIGTRAP, &sa, NULL);

#ifdef DDB
	sa.sa_sigaction = exc_debugger;
	sigaction(SIGQUIT, &sa, NULL);
#endif

	sa.sa_sigaction = ipi;
	if (sigaction(SIGUSR1, &sa, NULL) != 0)
	{
		warn("ipi handler setup failed");
		panic("IPI setup failed");
	}

	bzero(&sa, sizeof(sa));
	sigemptyset(&sa.sa_mask);
	sa.sa_flags |= SA_MAILBOX | SA_NODEFER;
	sa.sa_mailbox = &mdcpu->gd_shutdown;
	sigaction(SIGTERM, &sa, NULL);
}

/*
 * This function handles a segmentation fault.  
 *
 * XXX We assume that trapframe is a subset of ucontext.  It is as of
 *     this writing.
 */
static void
exc_segfault(int signo, siginfo_t *info, void *ctxp)
{
	ucontext_t *ctx = ctxp;

#if 0
	kprintf("CAUGHT SEGFAULT EIP %08x ERR %08x TRAPNO %d err %d\n",
		ctx->uc_mcontext.mc_eip,
		ctx->uc_mcontext.mc_err,
		ctx->uc_mcontext.mc_trapno & 0xFFFF,
		ctx->uc_mcontext.mc_trapno >> 16);
#endif
	kern_trap((struct trapframe *)&ctx->uc_mcontext.mc_gs);
	splz();
}

/*
 * This function runs in a thread dedicated to external shutdown signals.
 *
 * Currently, when a vkernel recieves a SIGTERM, either the VKERNEL init(8) 
 * is signaled with SIGUSR2, or the VKERNEL simply shuts down, preventing
 * fsck's when the VKERNEL is restarted.
 */ 
static void
sigshutdown_daemon( void )
{
	while (mdcpu->gd_shutdown == 0) {
		tsleep(&mdcpu->gd_shutdown, 0, "sswait", 0);
	}
	mdcpu->gd_shutdown = 0;
	kprintf("Caught SIGTERM from host system. Shutting down...\n");
	if (initproc != NULL) {
		ksignal(initproc, SIGUSR2);
	}
	else {
		reboot(RB_POWEROFF);
	}	
}
SYSINIT(sigshutdown, SI_BOOT2_PROC0, SI_ORDER_ANY, 
	kproc_start, &sigshut_kp); 

#ifdef DDB

static void
exc_debugger(int signo, siginfo_t *info, void *ctx)
{
	Debugger("interrupt from console");
}

#endif
